{
  nix = {
    binaryCaches = [
      "https://imlonghao.cachix.org"
    ];
    binaryCachePublicKeys = [
      "imlonghao.cachix.org-1:6QziXQCW4eXPMYaTQSoKrFteHYcyxjzapD+kOG4ONWk="
    ];
  };
}

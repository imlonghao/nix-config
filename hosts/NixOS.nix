{ pkgs, ... }:

let
  trojan-go-cfg = (builtins.fromJSON (builtins.readFile ../secrets/NixOS/trojan-go.json));
in
{
  ### root password is empty by default ###
  imports = [
    ../users/imlonghao
    ../users/root
    ./hardware/NixOS.nix
    ../local/input.nix
    ../profiles/yubikey
  ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.networkmanager.enable = true;
  networking.proxy.default = "socks5://127.0.0.1:1080";
  networking.useDHCP = false;
  networking.interfaces.enp3s0.useDHCP = true;
  networking.interfaces.wlp5s0.useDHCP = true;

  services.xserver.enable = true;
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;

  sound.enable = true;
  hardware.pulseaudio.enable = true;

  environment.systemPackages = with pkgs; [
    wget
    konsole
    dolphin
    tdesktop
    qliveplayer
    joplin-desktop
    tncattach
    synergy
    obsidian
    jetbrains.goland
    go
    libreoffice-qt
    remmina
    git-crypt
    burpsuite
    openvpn
    okular
  ];

  services.trojan-go = {
    enable = true;
    inherit (trojan-go-cfg) remote_addr remote_port password ssl;
  };

}

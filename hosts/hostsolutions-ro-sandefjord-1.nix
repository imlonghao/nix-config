{
  ### root password is empty by default ###
  imports = [
    ./hardware/hostsolutions-ro-sandefjord-1.nix
    ../profiles/openssh
    ../profiles/telegraf
    ../profiles/teleport
    ../users/root
  ];

  boot.cleanTmpDir = true;
  
  networking.firewall.allowPing = true;
  networking.firewall.allowedTCPPorts = [ 3022 ];
  networking.useDHCP = false;
  networking.interfaces.ens18.ipv4.addresses = [{
    address = "89.45.46.184";
    prefixLength = 24;
  }];
  networking.defaultGateway = "89.45.46.1";
  networking.nameservers = [ "1.1.1.1" "8.8.8.8" ];

  services.chrony.enable = true;
}

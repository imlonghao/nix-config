{ stdenv, lib, fetchFromGitHub, extra-cmake-modules, qtquickcontrols2
, makeWrapper, mpv, curl, ffmpeg, python3, python3Packages, wrapQtAppsHook }:

stdenv.mkDerivation rec {
  pname = "qliveplayer";
  version = "3.20.2";

  src = fetchFromGitHub {
    owner = "IsoaSFlus";
    repo = pname;
    rev = version;
    sha256 = "sha256-g/xecE+UaRIAUd6qc/JpZ/Pb8JJ+U/ACObYcqBcn/ds=";
  };

  buildInputs = [ makeWrapper mpv ];

  nativeBuildInputs = [ extra-cmake-modules qtquickcontrols2 wrapQtAppsHook ];

  cmakeFlags = [ "-DCMAKE_BUILD_TYPE=Release" ];

  postInstall = ''
    for program in $out/bin/*
    do
      wrapProgram $program \
        --prefix PATH : ${lib.makeBinPath [ mpv curl ffmpeg python3 ]} \
        --prefix PYTHONPATH : $PYTHONPATH:$(toPythonPath python3Packages.aiohttp)
    done
  '';

  meta = with lib; {
    description = "A cute and useful Live Stream Player with danmaku support";
    homepage = "https://github.com/IsoaSFlus/QLivePlayer";
    license = licenses.gpl2Only;
  };
}

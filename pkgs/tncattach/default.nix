{ stdenv, fetchFromGitHub, lib, installShellFiles }:

stdenv.mkDerivation rec {
  pname = "tncattach";
  version = "0.1.9";

  src = fetchFromGitHub {
    owner = "markqvist";
    repo = pname;
    rev = "${version}";
    sha256 = "sha256-s1uvUq9Y3/58wh3azcCu3KDxHQ1FHxMFX+rdjR9p6lg=";
  };

  nativeBuildInputs = [ installShellFiles ];

  installPhase = ''
    install -Dm755 $pname $out/bin/$pname
    installManPage $pname.8
  '';

  meta = with lib; {
    description = "Attach TNC devices as network interfaces";
    homepage = "https://github.com/markqvist/tncattach";
    license = licenses.mit;
  };
}

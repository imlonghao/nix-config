{ buildGoModule, fetchFromGitHub, lib }:

buildGoModule rec {
  pname = "trojan-go";
  version = "0.8.2";

  src = fetchFromGitHub {
    owner = "p4gefau1t";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-hPW3ZZrLYin69j8TK+yszNIt2JY0Q72I0vBQ5gH9dUE=";
  };

  subPackages = [ "." ];

  vendorSha256 = "sha256-SXUVgNBDe+EAnpxhhEXarLRFSghvfY6ubHveGJ6CVQo=";

  doCheck = false;

  buildFlags = [ "-tags" "full" ];

  meta = with lib; {
    description = "A Trojan proxy written in Go";
    homepage = "https://github.com/p4gefau1t/trojan-go";
    license = licenses.gpl3Only;
  };
}

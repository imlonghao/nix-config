final: prev: {
  trojan-go = prev.callPackage ./trojan-go { };
  qliveplayer = prev.libsForQt5.callPackage ./qliveplayer { };
  tncattach = prev.callPackage ./tncattach { };
  fcitx5-pinyin-moegirl = prev.callPackage ./fcitx5/fcitx5-pinyin-moegirl { };
  fcitx5-pinyin-zhwiki = prev.callPackage ./fcitx5/fcitx5-pinyin-zhwiki { };
}

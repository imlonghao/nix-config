{ stdenv, fetchurl, lib, installShellFiles }:

stdenv.mkDerivation rec {
  pname = "fcitx5-pinyin-zhwiki";
  version = "20210101";

  src = fetchurl {
    url = "https://github.com/felixonmars/fcitx5-pinyin-zhwiki/releases/download/0.2.2/zhwiki-${version}.dict";
    sha256 = "sha256-HCwQ7RekneUXqXfYmRiJ93CX+Oscn8gmAU+sIKqYJwQ=";
  };

  phases = [ "installPhase" ];

  nativeBuildInputs = [ installShellFiles ];

  installPhase = ''
    install -Dm644 $src $out/share/fcitx5/pinyin/dictionaries/zhwiki.dict
  '';

  meta = with lib; {
    description = "Fcitx 5 Pinyin Dictionary from zh.wikipedia.org";
    homepage = "https://github.com/felixonmars/fcitx5-pinyin-zhwiki";
    license = licenses.cc-by-sa-30;
  };
}

{ stdenv, fetchurl, lib, installShellFiles }:

stdenv.mkDerivation rec {
  pname = "fcitx5-pinyin-moegirl";
  version = "20210114";

  src = fetchurl {
    url = "https://github.com/outloudvi/mw2fcitx/releases/download/${version}/moegirl.dict";
    sha256 = "sha256-HZabFVvobT7QnKIak+cyILOHIWhdfxNmpPyLrnD4nGo=";
  };

  phases = [ "installPhase" ];

  nativeBuildInputs = [ installShellFiles ];

  installPhase = ''
    install -Dm644 $src $out/share/fcitx5/pinyin/dictionaries/moegirl.dict
  '';

  meta = with lib; {
    description = "Fcitx 5 Pinyin Dictionary from zh.moegirl.org.cn";
    homepage = "https://github.com/outloudvi/mw2fcitx";
    license = licenses.cc-by-nc-sa-30;
  };
}

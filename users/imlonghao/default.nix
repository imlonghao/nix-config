{ pkgs, ... }:

{
  users.users.imlonghao = {
    uid = 1000;
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    hashedPassword = (builtins.readFile ../../secrets/imlonghao);
  };
  home-manager.users.imlonghao = {
    programs.chromium = {
      enable = true;
      extensions = 
      [
        "padekgcemlokbadohgkifijomclgjgif" # Proxy SwitchyOmega
        "bkhaagjahfmjljalopjnoealnfndnagc" # Octotree
        "cfhdojbkjhnklbpkdaibdccddilifddb" # Adblock Plus
        "kbfnbcaeplbcioakkpcpgfkobkghlhen" # Grammarly
        "nngceckbapebfimnlniiiahkandclblb" # Bitwarden
        "dhdgffkkebhmkfjojejmpbldmpobfkfo" # Tampermonkey
        "eningockdidmgiojffjmkdblpjocbhgh" # Header Editor
      ];
    };
    programs.git = {
      enable = true;
      userEmail = "git@imlonghao.com";
      userName = "imlonghao";
    };
    programs.vscode = {
      enable = true;
      extensions = with pkgs.vscode-extensions; [
        golang.Go ms-python.python bbenoist.Nix
      ];
    };
    programs.mpv = {
      enable = true;
    };
    services.flameshot.enable = true;
    
  };
}

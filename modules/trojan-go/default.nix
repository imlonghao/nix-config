{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.trojan-go;
  configfile = pkgs.writeText "config.json" (builtins.toJSON {
    inherit (cfg) run_type local_addr local_port remote_addr remote_port password ssl;
  });
in
{
  options.services.trojan-go = {
    enable = mkOption { type = types.bool; default = false; };
    run_type = mkOption { type = types.str; default = "client"; };
    local_addr = mkOption { type = types.str; default = "127.0.0.1"; };
    local_port = mkOption { type = types.int; default = 1080; };
    remote_addr = mkOption { type = types.str; };
    remote_port = mkOption { type = types.int; };
    password = mkOption { type = types.listOf types.str; };
    ssl = {
      sni = mkOption { type = types.str; };
    };
  };
  config = mkIf cfg.enable {
    environment.systemPackages = [ pkgs.trojan-go ];
    systemd.services.trojan-go = {
      enable = true;
      description = "Trojan-Go - An unidentifiable mechanism that helps you bypass GFW";
      after = [ "network.target" "nss-lookup.target" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        ExecStart = "${pkgs.trojan-go}/bin/trojan-go -config='${configfile}'";
        Restart = "on-failure";
        RestartSec = "10s";
        CapabilityBoundingSet = [ "CAP_NET_ADMIN" "CAP_NET_BIND_SERVICE" ];
        AmbientCapabilities = [ "CAP_NET_ADMIN" "CAP_NET_BIND_SERVICE" ];
        NoNewPrivileges = true;
      };
    };
  };
}
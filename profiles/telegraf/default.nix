{ config, ... }:

let
  cfg = (builtins.fromJSON (builtins.readFile ../../secrets/telegraf.json));
in
{
  services.telegraf = {
    enable = true;
    extraConfig = {
      inputs = {
        cpu = {
          percpu = true;
          totalcpu = true;
          collect_cpu_time = false;
          report_active = false;
        };
        disk = {
          ignore_fs =
            [ "tmpfs" "devtmpfs" "devfs" "iso9660" "overlay" "aufs" "squashfs" ];
        };
        diskio = { };
        kernel = { };
        mem = { };
        processes = { };
        swap = { };
        system = { };
        net = { interfaces = cfg.interface."${config.networking.hostName}"; };
        netstat = { };
      };
      outputs = {
        influxdb = {
          urls = [ cfg.influxdb.url ];
          skip_database_creation = true;
          username = cfg.influxdb.username;
          password = cfg.influxdb.password;
        };
      };
    };
  };
}
